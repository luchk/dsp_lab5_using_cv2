import cv2
from PIL import Image
import matplotlib.pyplot as plt
import numpy as np
import random
path='/Users/viktor/Desktop/lab5.jpg'
# image = cv2.imread(path)
def red(image):
    r = image.copy()
    # set blue and green channels to 0
    r[:, :, 0] = 0
    r[:, :, 1] = 0
    cv2.imshow('R-RGB', r)
    cv2.waitKey(0)
def grean(image):
    g = image.copy()
    # set blue and red channels to 0
    g[:, :, 0] = 0
    g[:, :, 2] = 0
    cv2.imshow('G-RGB', g)
    cv2.waitKey(0)
def blue(image):
    b = image.copy()
    # set green and red channels to 0
    b[:, :, 1] = 0
    b[:, :, 2] = 0
    cv2.imshow('B-RGB', b)
    cv2.waitKey(0)

def get_resolution(path):
    with Image.open(path) as img:
        width, height = img.size
        return width, height

def resize(w,h,path):
    im=cv2.imread(path)
    cv2.imshow('original',im)
    img=cv2.resize(im,(int(w/2),int(h/2)))
    cv2.imshow('resized',img)
    cv2.waitKey(0)
def bw(path):
    im=cv2.imread(path,0)
    cv2.imshow('bw',im)
    cv2.waitKey(0)
def histogram(path):
    img = cv2.imread(path)
    plt.hist(img.ravel(), 256)
    plt.show()
def eq_histogram_show(img,img2):
    plt.hist(img.ravel(), 256)
    plt.hist(img2.ravel(), 256)
    plt.show()
def hist_equalize(path):
    img = cv2.imread(path, 0)
    equ = cv2.equalizeHist(img)
    eq_histogram_show(equ, img)
def hist_equal_with_step(path):
    img = cv2.imread(path, 0)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    cl1 = clahe.apply(img)
    plt.hist(cl1.ravel(), 256)
    plt.show()
def noise(per,path):
    w, h = get_resolution(path)
    image = cv2.imread(path)
    noise=((w*h)/100)*per
    for i in range(int(noise)):
        i=random.randint(0, int(h-1))
        j=random.randint(0, int(w-1))
        bw=random.randint(1,2)
        if bw==1:
            n=0
        else:
            n=255
        image[int(i)][int(j)]=int(n)
    cv2.imshow('with noise', image)
    cv2.waitKey(0)
noise(5,path)